# Slovak messages for release-notes.
# Copyright (C) 2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2012, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2019-07-07 21:17+0200\n"
"PO-Revision-Date: 2017-06-16 15:21+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <book>
#: en/release-notes.dbk:8
msgid "en"
msgstr "sk"

#. type: Content of: <book><title>
#: en/release-notes.dbk:9
msgid "Release Notes for &debian; &release; (&releasename;), &arch-title;"
msgstr "Poznámky k vydaniu &debian; &release; (&releasename;), &arch-title;"

#. type: Content of: <book><subtitle>
#: en/release-notes.dbk:11
#, fuzzy
#| msgid ""
#| "<ulink url=\"http://www.debian.org/doc/\">The Debian Documentation "
#| "Project</ulink>"
msgid ""
"<ulink url=\"https://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"
msgstr ""
"<ulink url=\"http://www.debian.org/doc/\">Dokumentačný projekt Debianu</"
"ulink>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:17
msgid ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"
msgstr ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:22
msgid ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"
msgstr ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:27
msgid ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"
msgstr ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:32
msgid ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"
msgstr ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:37
msgid "<firstname></firstname> <surname></surname>"
msgstr "<firstname></firstname> <surname></surname>"

#. type: Content of: <book><bookinfo><editor><contrib>
#: en/release-notes.dbk:39
msgid "There were more people!"
msgstr "A ďalší!"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:43
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."
msgstr ""
"Tento dokument je slobodný softvér; môžete ho šíriť a/alebo meniť za "
"podmienok licencie GNU General Public License verzie 2 ako ju publikovala "
"Free Software Foundation."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:49
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"Tento program je šírený vo viere, že bude užitočný, ale BEZ AKEJKOĽVEK "
"ZÁRUKY; dokonca aj bez implicitnej záruky OBCHODOVATEĽNOSTI či VHODNOSTI NA "
"URČITÝ ÚČEL. Podrobnosti nájdete v GNU General Public License."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:56
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Spolu s týmto programom by ste mali dostať kópiu GNU General Public License; "
"ak nie, napíšte na adresu Free Software Foundation, Inc., 51 Franklin "
"Street, Fifth Floor, Boston, MA 02110-1301 USA."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:61
#, fuzzy
#| msgid ""
#| "The license text can also be found at <ulink url=\"http://www.gnu.org/"
#| "licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
#| "filename> on &debian;."
msgid ""
"The license text can also be found at <ulink url=\"https://www.gnu.org/"
"licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
"filename> on &debian; systems."
msgstr ""
"Text licencie tiež môžete nájsť na <ulink url=\"http://www.gnu.org/licenses/"
"gpl-2.0.html\"/> a v súbore <filename>/usr/share/common-licenses/GPL-2</"
"filename> v Debiane."

#. type: Content of: <book><appendix><title>
#: en/release-notes.dbk:84
msgid "Contributors to the Release Notes"
msgstr "Prispievatelia do Poznámok k vydaniu"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:86
msgid ""
"Many people helped with the release notes, including, but not limited to"
msgstr "Vzniku týchto Poznámok k vydaniu pomohli mnohí ľudia, okrem iných aj"

#.  alphabetical (LANG=C sort) order by firstname 
#.  the contrib will not be printed, but is a reminder for the editor;
#.          username as shown in vcs log, contribution  
#.  list of translators will only show up in translated texts, only list
#.          contributors to en/ here 
#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:94
#, fuzzy
#| msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgid "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:97
msgid "various fixes in 2013"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:98
#, fuzzy
#| msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgid ""
"</author>, <author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:102 en/release-notes.dbk:217
msgid "previous releases"
msgstr "predošlé vydania"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:103
msgid ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"
msgstr ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:107
msgid "aba, previous releases: 2005 - 2007"
msgstr "aba, predošlé vydania: 2005 - 2007"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:108
msgid ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"
msgstr ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:112 en/release-notes.dbk:152 en/release-notes.dbk:252
#: en/release-notes.dbk:272
msgid "various contributions"
msgstr "rôzne príspevky"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:113
msgid ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"
msgstr ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:117 en/release-notes.dbk:122
msgid "previous release"
msgstr "predošlé vydanie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:118
msgid ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"
msgstr ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:123
msgid ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"
msgstr ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:127
msgid "description of GM965 issue"
msgstr "popis problému GM965"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:128
msgid ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:132
msgid "bubulle, Lenny installation"
msgstr "bubulle, inštalácia Lenny"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:133
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"
msgstr ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:137
msgid "PostgreSQL-specific issues"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:138
msgid ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:142
msgid "Debian Live"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:143
msgid ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"
msgstr ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:147
msgid "taffit, Wheezy release"
msgstr "taffit, vydanie Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:148
msgid ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"
msgstr ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:153
msgid ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"
msgstr ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:157
msgid "backports"
msgstr "spätné porty"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:158
msgid ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"
msgstr ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:162
msgid "rework X11 upgrade"
msgstr "prepracovaná aktualizácia X11"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:163
msgid "</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgstr ""
"</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:167
msgid "fjp, previous release (Etch)"
msgstr "fjp, predošlé vydanie (Etch)"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:168
msgid ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:172 en/release-notes.dbk:262
msgid "innumerable contributions"
msgstr "nespočetné príspevky"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:173
msgid ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:177 en/release-notes.dbk:242
msgid "ARM port issues"
msgstr "problémy s portom ARM"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:178
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Hideki</firstname> <surname>Yamane</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:182
msgid "henrich, contributed and contributing since 2006"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:183
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgid ""
"</author>, <author> <firstname>Holger</firstname> <surname>Wansing</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:187
msgid "holgerw, contributed and contributing since 2009"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:188
msgid ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"
msgstr ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:192
msgid "jfs, Etch release, Squeeze release"
msgstr "jfs, vydanie Etch, vydanie Squeeze"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:193
msgid ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgstr ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:197
msgid "German translation, innumerable contributions"
msgstr "nemecký preklad, nespočetné príspevky"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:198
msgid ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgstr ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:202 en/release-notes.dbk:247
msgid "syslog issues"
msgstr "problémy so syslog"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:203
msgid ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"
msgstr ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:207
msgid "jrnieder@gmail.com, Squeeze release, Wheezy release"
msgstr "jrnieder@gmail.com, vydanie Squeeze, vydanie Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:208
msgid ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:212
msgid "joostvb, Wheezy release, Jessie release"
msgstr "joostvb, vydanie Wheezy, vydanie Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:213
msgid ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgstr ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:218
msgid ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"
msgstr ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:222
msgid "jcristau, Squeeze release, Wheezy release"
msgstr "jcristau, vydanie Squeeze, vydanie Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:223
msgid ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"
msgstr ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:227
msgid "English fixes"
msgstr "opravy anglického originálu"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:228
msgid ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"
msgstr ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:232
msgid "description of NFS issues"
msgstr "popis problémov s NFS"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:233
msgid "</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"
msgstr ""
"</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:237
msgid "editors motivation manager"
msgstr "správca motivácie redaktorov"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:238
msgid ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:243
msgid ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"
msgstr ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:248
msgid ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:253
msgid ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"
msgstr ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:257
msgid "nthykier, Jessie release"
msgstr "nthykier, vydanie Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:258
msgid ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"
msgstr ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:263
msgid ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:267
msgid "Japanese translation (coordination), innumerable contributions"
msgstr "japonský preklad (koordinátor), nespočetné príspevky"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:268
msgid ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"
msgstr ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:273
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgid ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:277
#, fuzzy
#| msgid "nthykier, Jessie release"
msgid "elbrus, buster release"
msgstr "nthykier, vydanie Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:278
msgid ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:282
msgid "kernel version note"
msgstr "poznámka o verzii jadra"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:283
msgid ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"
msgstr ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:287 en/release-notes.dbk:312
msgid "Etch release"
msgstr "vydanie Etch"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:288
msgid ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"
msgstr ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:292 en/release-notes.dbk:297
msgid "description of d-i Braille support"
msgstr "popis podpory braillovho písma v d-i"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:293
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:298
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:302
msgid "spaillar-guest, innumerable contributions"
msgstr "spaillar-guest, nespočetné príspevky"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:303
msgid ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"
msgstr ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:307
msgid "description of Apache issues"
msgstr "popis problémov s Apache"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:308
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:313
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:317
msgid "Debian CDs"
msgstr "CD Debianu"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:318
msgid ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"
msgstr ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:322 en/release-notes.dbk:331
msgid "description of \"proposed-update\""
msgstr "popis „proposed-update“"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:323
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgid "</author>, <author> <firstname>victory</firstname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:326
msgid ""
"victory-guest victory.deb@gmail.com, markup fixes, contributed and "
"contributing since 2006"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:327
msgid ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:332
msgid ""
"</author>, and <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"
msgstr ""
"</author> a <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:337
msgid "editing Lenny release, switch to DocBook XML"
msgstr "úprava vydania pre Lenny, prevod na DocBook XML"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:338
msgid "</author>."
msgstr "</author>."

#.  translator names here, depending on language!
#.     </para>
#. <para>Translated into Klingon by:
#.     <author>
#.       <firstname>Firstname1</firstname>
#.       <surname>Surname1</surname>
#.       <contrib>Foo translation</contrib>
#.     </author>,
#.     <author>
#.       <firstname>Firstname2</firstname>
#.       <surname>Surname2</surname>
#.       <contrib>Foo translation</contrib>
#.     </author 
#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:341
msgid ""
"This document has been translated into many languages.  Many thanks to the "
"translators!"
msgstr ""
"Tento dokument bol preložený do mnohých jazykov. Vďaka prekladateľom!\n"
"</para><para>Do slovenčiny preložil:\n"
"   <author>\n"
"     <firstname>Ivan</firstname>\n"
"     <surname>Masár</surname>\n"
"     <email>helix84@centrum.sk</email>\n"
"   </author>."

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:364
msgid "ACPI"
msgstr "ACPI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:365
msgid "Advanced Configuration and Power Interface"
msgstr ""
"Advanced Configuration and Power Interface (pokročilé rozhranie na správu "
"konfigurácie a napájania)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:368
msgid "ALSA"
msgstr "ALSA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:369
msgid "Advanced Linux Sound Architecture"
msgstr ""
"Advanced Linux Sound Architecture (pokročilá zvuková architektúra Linuxu)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:372
msgid "BD"
msgstr "BD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:373
msgid "Blu-ray Disc"
msgstr "Blu-ray Disc (disk Blu-ray)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:376
msgid "CD"
msgstr "CD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:377
msgid "Compact Disc"
msgstr "Compact Disc (disk CD)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:380
msgid "CD-ROM"
msgstr "CD-ROM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:381
msgid "Compact Disc Read Only Memory"
msgstr "Compact Disc Read Only Memory (disk CD-ROM, len na čítanie)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:384
msgid "DHCP"
msgstr "DHCP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:385
msgid "Dynamic Host Configuration Protocol"
msgstr ""
"Dynamic Host Configuration Protocol (protokol na dynamickú konfiguráciu "
"počítačov)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:388
msgid "DLBD"
msgstr "DLBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:389
msgid "Dual Layer Blu-ray Disc"
msgstr "Dual Layer Blu-ray Disc (dvojvrstvový disk Blu-ray)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:392
msgid "DNS"
msgstr "DNS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:393
msgid "Domain Name System"
msgstr "Domain Name System (systém názvov domén)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:396
msgid "DVD"
msgstr "DVD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:397
msgid "Digital Versatile Disc"
msgstr "Digital Versatile Disc (disk DVD)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:400
msgid "GIMP"
msgstr "GIMP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:401
msgid "GNU Image Manipulation Program"
msgstr "GNU Image Manipulation Program (program GNU na prácu s obrázkami)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:404
msgid "GNU"
msgstr "GNU"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:405
msgid "GNU's Not Unix"
msgstr "GNU's Not Unix (GNU nie je Unix)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:408
msgid "GPG"
msgstr "GPG"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:409
msgid "GNU Privacy Guard"
msgstr "GNU Privacy Guard (program GNU na ochranu súkromia)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:412
msgid "LDAP"
msgstr "LDAP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:413
msgid "Lightweight Directory Access Protocol"
msgstr ""
"Lightweight Directory Access Protocol (nenáročný protokol na prístup k "
"adresárom)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:416
msgid "LSB"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:417
msgid "Linux Standard Base"
msgstr "Linux Standard Base (štandardný základ Linuxu)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:420
msgid "LVM"
msgstr "LVM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:421
msgid "Logical Volume Manager"
msgstr "Logical Volume Manager (správca logických zväzkov)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:424
msgid "MTA"
msgstr "MTA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:425
msgid "Mail Transport Agent"
msgstr "Mail Transport Agent (agent na prenos pošty)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:428
msgid "NBD"
msgstr "NBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:429
msgid "Network Block Device"
msgstr "Network Block Device (sieťové blokové zariadenie)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:432
msgid "NFS"
msgstr "NFS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:433
msgid "Network File System"
msgstr "Network File System (sieťový súborový systém)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:436
msgid "NIC"
msgstr "NIC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:437
msgid "Network Interface Card"
msgstr "Network Interface Card (karta sieťového rozhrania)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:440
msgid "NIS"
msgstr "NIS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:441
msgid "Network Information Service"
msgstr "Network Information Service (sieťová informačná služba)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:444
msgid "PHP"
msgstr "PHP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:445
msgid "PHP: Hypertext Preprocessor"
msgstr "PHP: Hypertext Preprocessor (PHP - preprocesor hypertextu)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:448
msgid "RAID"
msgstr "RAID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:449
msgid "Redundant Array of Independent Disks"
msgstr ""
"Redundant Array of Independent Disks (redundantné (nadbytočné) pole "
"nezávislých diskov)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:452
msgid "SATA"
msgstr "SATA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:453
msgid "Serial Advanced Technology Attachment"
msgstr "Serial Advanced Technology Attachment"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:456
msgid "SSL"
msgstr "SSL"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:457
msgid "Secure Sockets Layer"
msgstr "Secure Sockets Layer (zabezpečená vrstva socketov)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:460
msgid "TLS"
msgstr "TLS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:461
msgid "Transport Layer Security"
msgstr "Transport Layer Security (zabezpečenie transportnej vrstvy)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:464
msgid "UEFI"
msgstr "UEFI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:465
msgid "Unified Extensible Firmware Interface"
msgstr ""
"Unified Extensible Firmware Interface (zjednotené rozšíriteľné firmvérové "
"rozhranie)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:468
msgid "USB"
msgstr "USB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:469
msgid "Universal Serial Bus"
msgstr "Universal Serial Bus (univerzálna sériové zbernica)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:472
msgid "UUID"
msgstr "UUID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:473
msgid "Universally Unique Identifier"
msgstr "Universally Unique Identifier (univerzálny jedinečný identifikátor)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:476
msgid "WPA"
msgstr "WPA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:477
msgid "Wi-Fi Protected Access"
msgstr "Wi-Fi Protected Access (chránený prístup k Wi-Fi)"

#~ msgid "APM"
#~ msgstr "APM"

#~ msgid "Advanced Power Management"
#~ msgstr "Advanced Power Management (pokročilá správa napájania)"

#~ msgid "IDE"
#~ msgstr "IDE"

#~ msgid "Integrated Drive Electronics"
#~ msgstr "Integrated Drive Electronics"

#~ msgid "LILO"
#~ msgstr "LILO"

#~ msgid "LInux LOader"
#~ msgstr "LInux LOader (zavádzač Linuxu)"

#~ msgid "OSS"
#~ msgstr "OSS"

#~ msgid "Open Sound System"
#~ msgstr "Open Sound System (otvorený zvukový systém)"

#~ msgid "RPC"
#~ msgstr "RPC"

#~ msgid "Remote Procedure Call"
#~ msgstr "Remote Procedure Call (volanie vzdialených procedúr)"

#~ msgid "VGA"
#~ msgstr "VGA"

#~ msgid "Video Graphics Array"
#~ msgstr "Video Graphics Array"

#~ msgid "previous release (Etch)"
#~ msgstr "predošlé vydanie (Etch)"

#~ msgid "February 4th, 2011"
#~ msgstr "4. február 2011"

#~ msgid ""
#~ "<editor> <firstname>Steve</firstname> <surname>Langasek</surname> "
#~ "<email>vorlon@debian.org</email> </editor> <editor> <firstname>W. Martin</"
#~ "firstname> <surname>Borgert</surname> <email>debacle@debian.org</email> </"
#~ "editor> <editor> <firstname>Javier</firstname> <surname>Fernandez-"
#~ "Sanguino</surname> <email>jfs@debian.org</email> </editor> <editor> "
#~ "<firstname>Julien</firstname> <surname>Cristau</surname> "
#~ "<email>jcristau@debian.org</email> </editor> <editor condition=\"fixme\"> "
#~ "<firstname></firstname> <surname></surname>"
#~ msgstr ""
#~ "<editor> <firstname>Steve</firstname> <surname>Langasek</surname> "
#~ "<email>vorlon@debian.org</email> </editor> <editor> <firstname>W. Martin</"
#~ "firstname> <surname>Borgert</surname> <email>debacle@debian.org</email> </"
#~ "editor> <editor> <firstname>Javier</firstname> <surname>Fernandez-"
#~ "Sanguino</surname> <email>jfs@debian.org</email> </editor> <editor> "
#~ "<firstname>Julien</firstname> <surname>Cristau</surname> "
#~ "<email>jcristau@debian.org</email> </editor> <editor condition=\"fixme\"> "
#~ "<firstname></firstname> <surname></surname>"

#~ msgid "</editor>"
#~ msgstr "</editor>"

#~| msgid "2009-02-14"
#~ msgid "2010-11-12"
#~ msgstr "2009-02-14"

#~ msgid "Lenny dedicated to Thiemo Seufer"
#~ msgstr "Lenny je venovaný Thiemovi Seuferovi"

#~ msgid ""
#~ "The Debian Project has lost an active member of its community. Thiemo "
#~ "Seufer died on December 26th, 2008 in a tragic car accident."
#~ msgstr ""
#~ "Projekt Debian stratil aktívneho člena svojej komunity. Thiemo Seufer "
#~ "zomrel 26. decembra 2008 pri tragickej autonehode."

#~ msgid ""
#~ "Thiemo was involved in Debian in many ways. He maintained several "
#~ "packages and was the main supporter of the Debian ports to the MIPS "
#~ "architecture. He was also a member of our kernel team, as well as a "
#~ "member of the Debian Installer team. His contributions reached far beyond "
#~ "the Debian project: he also worked on the MIPS port of the Linux kernel, "
#~ "the MIPS emulation of qemu, and far too many smaller projects to be named "
#~ "here."
#~ msgstr ""
#~ "Thiemo sa zapájal do Debianu mnohými spôsobmi. Spravoval niekoľko balíkov "
#~ "a bol hlavným podporovateľom portu Debianu na architektúru MIPS. Tiež bol "
#~ "členom nášho tímu jadra a členom tímu inštalátora Debianu. Jeho príspevky "
#~ "siahali ďaleko za projekt Debian: tiež spolupracoval na porte jadra "
#~ "Linuxu na MIPS, na emuláciu MIPS v qemu a mnohých ďalších menších "
#~ "projektoch."

#~ msgid ""
#~ "Thiemo's work, dedication, broad technical knowledge and ability to share "
#~ "this with others will be missed. His contributions will not be "
#~ "forgotten.  The high standards of Thiemo's work make it hard to pick up."
#~ msgstr ""
#~ "Thiemova práca, oddanosť a široké technické vedomosti a schopnosť podeliť "
#~ "sa o ne s ostatnými nám budú chýbať. Jeho príspevky nezabudneme. Vysoké "
#~ "štandardy Thiemovej práce bude ťažké udržať."

#~ msgid ""
#~ "To honour his contributions to Debian, the project dedicates the release "
#~ "of Debian GNU/Linux 5.0 <quote>Lenny</quote> to Thiemo."
#~ msgstr ""
#~ "Na počesť jeho príspevkov do Debianu, projekt venuje vydanie Debian GNU/"
#~ "Linux 5.0 <quote>Lenny</quote> Thiemovi."

#, fuzzy
#~ msgid ""
#~ "</author>, <author> <firstname>Firstname2</firstname> <surname>Surname2</"
#~ "surname>"
#~ msgstr ""
#~ "</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#, fuzzy
#~ msgid "</author>"
#~ msgstr "</author>."
