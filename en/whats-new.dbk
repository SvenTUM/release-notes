<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-whats-new" lang="en">
<title>What's new in &debian; &release;</title>
<para>
  The <ulink url="&url-wiki-newinrelease;">Wiki</ulink> has more information
about this topic.
</para>

<!--
Sources for architecture status:
  https://release.debian.org/buster/arch_qualify.html

Some descriptions of the ports: https://www.debian.org/ports/
-->
<section>
<title>Supported architectures</title>

<para>
The following are the officially supported architectures for &debian;
&release;:
</para>
<itemizedlist>
<listitem>
<para>
32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit ARM (<literal>arm64</literal>)
</para>
</listitem>
<listitem>
<para>
ARM EABI (<literal>armel</literal>)
</para>
</listitem>
<listitem>
<para>
ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)
</para>
</listitem>
<listitem>
<para>
little-endian MIPS (<literal>mipsel</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian MIPS (<literal>mips64el</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian PowerPC (<literal>ppc64el</literal>)
</para>
</listitem>
<listitem>
<para>
IBM System z (<literal>s390x</literal>)
</para>
</listitem>
</itemizedlist>

<para>
You can read more about port status, and port-specific information for your
architecture at the <ulink url="&url-ports;">Debian port
web pages</ulink>.
</para>

</section>

<section id="newdistro">
<title>What's new in the distribution?</title>

<programlisting condition="fixme">
 TODO: Make sure you update the numbers in the .ent file
     using the changes-release.pl script found under ../
</programlisting>

<para>
This new release of Debian again comes with a lot more software than
its predecessor &oldreleasename;; the distribution includes over
&packages-new; new packages, for a total of over &packages-total;
packages.  Most of the software in the distribution has been updated:
over &packages-updated; software packages (this is
&packages-update-percent;% of all packages in &oldreleasename;).
Also, a significant number of packages (over &packages-removed;,
&packages-removed-percent;% of the packages in &oldreleasename;) have
for various reasons been removed from the distribution.  You will not
see any updates for these packages and they will be marked as
"obsolete" in package management front-ends; see <xref
linkend="obsolete"/>.
</para>

<para>
  &debian; again ships with several desktop applications and
  environments.  Among others it now includes the desktop environments
  GNOME<indexterm><primary>GNOME</primary></indexterm> 3.30,
  KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.14,
  LXDE<indexterm><primary>LXDE</primary></indexterm> 10,
  LXQt<indexterm><primary>LXQt</primary></indexterm> 0.14,
  MATE<indexterm><primary>MATE</primary></indexterm> 1.20, and
  Xfce<indexterm><primary>Xfce</primary></indexterm> 4.12.
</para>
<para>
  Productivity applications have also been upgraded, including the
  office suites:
</para>
  <itemizedlist>
  <listitem>
    <para>
      LibreOffice<indexterm><primary>LibreOffice</primary></indexterm>
      is upgraded to version 6.1;
    </para>
  </listitem>
  <listitem>
    <para>
      Calligra<indexterm><primary>Calligra</primary></indexterm>
      is upgraded to 3.1.
    </para>
  </listitem>
  <listitem>
    <para>
      GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 3.4;
    </para>
  </listitem>
  <!-- no updates?
  <listitem>
    <para>
      GNUmeric<indexterm><primary>GNUmeric</primary></indexterm> is upgraded to 1.12;
    </para>
  </listitem>
  <listitem>
    <para>
      Abiword<indexterm><primary>Abiword</primary></indexterm> is upgraded to 3.0.
      </listitem>
      </ -->
  </itemizedlist>

<para>
  Updates of other desktop applications include the upgrade to
  Evolution<indexterm><primary>Evolution</primary></indexterm> 3.30.
</para>

<!-- JFS:
Might it be useful point to https://distrowatch.com/table.php?distribution=debian ?
This provides a more comprehensive comparison among different releases -->

<para>
Among many others, this release also includes the following software updates:
</para>
<informaltable pgwide="1">
  <tgroup cols="3">
    <colspec align="justify"/>
    <colspec align="justify"/>
    <colspec align="justify"/>
    <!-- colspec align="justify" colwidth="3*"/ -->
    <thead>
      <row>
	<entry>Package</entry>
	<entry>Version in &oldrelease; (&oldreleasename;)</entry>
	<entry>Version in &release; (&releasename;)</entry>
      </row>
    </thead>
    <tbody>
      <row id="new-apache2">
	<entry>Apache<indexterm><primary>Apache</primary></indexterm></entry>
	<entry>2.4.38</entry>
	<entry>2.4.46</entry>
      </row>
      <row id="new-bind9">
	<entry>BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> Server</entry>
	<entry>9.11</entry>
	<entry>9.16</entry>
      </row>
<!--
      <row id="new-chromium">
	<entry>Chromium<indexterm><primary>Chromium</primary></indexterm></entry>
	<entry>53.0</entry>
	<entry>73.0</entry>
      </row>
      <row id="new-courier">
	<entry>Courier<indexterm><primary>Courier</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>0.73</entry>
	<entry>1.0</entry>
      </row>
-->
      <row id="new-cryptsetup">
        <entry>Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm></entry>
	<entry>2.1</entry>
	<entry>2.3</entry>
      </row>
<!--
      <row id="new-dia">
	<entry>Dia<indexterm><primary>Dia</primary></indexterm></entry>
	<entry>0.97.2</entry>
	<entry>0.97.3</entry>
        </row>
-->
      <row id="new-dovecot">
	<entry>Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>2.3.4</entry>
	<entry>2.3.13</entry>
      </row>
      <row id="new-emacs">
	<entry>Emacs</entry>
	<entry>26.1</entry>
	<entry>27.1</entry>
      </row>
      <row id="new-exim4">
	<entry>Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server</entry>
	<entry>4.92</entry>
	<entry>4.94</entry>
      </row>
<!--
      <row id="new-firefox">
	<entry>Firefox<indexterm><primary>Firefox</primary></indexterm></entry>
	<entry>45.5 (AKA Iceweasel)</entry>
	<entry>60.7 (ESR)</entry>
      </row>
-->
      <row id="new-gcc">
	<entry><acronym>GNU</acronym> Compiler Collection as default compiler<indexterm><primary>GCC</primary></indexterm></entry>
	<entry>8.3</entry>
	<entry>10.2</entry>
      </row>
      <row id="new-gimp">
	<entry><acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm></entry>
	<entry>2.10.8</entry>
	<entry>2.10.22</entry>
      </row>
      <row id="new-gnupg">
	<entry>GnuPG<indexterm><primary>GnuPG</primary></indexterm></entry>
	<entry>2.2.12</entry>
	<entry>2.2.20</entry>
      </row>
      <row id="new-inkscape">
	<entry>Inkscape<indexterm><primary>Inkscape</primary></indexterm></entry>
	<entry>0.92.4</entry>
	<entry>1.0.2</entry>
      </row>
      <row id="new-libc6">
	<entry>the <acronym>GNU</acronym> C library</entry>
	<entry>2.28</entry>
	<entry>2.31</entry>
      </row>
      <row id="new-lighttpd">
	<entry>lighttpd</entry>
	<entry>1.4.53</entry>
	<entry>1.4.58</entry>
      </row>
      <row id="new-linux-image">
        <entry>Linux kernel image</entry>
        <entry>4.19 series</entry>
        <entry>5.10 series</entry>
      </row>
      <row id="llvm-toolchain">
        <entry>LLVM/Clang toolchain</entry>
        <entry>6.0.1 and 7.0.1 (default)</entry>
        <entry>9.0.1 and 11.0.1 (default)</entry>
      </row>
      <row id="new-mariadb">
	<entry>MariaDB<indexterm><primary>MariaDB</primary></indexterm></entry>
	<entry>10.3</entry>
	<entry>10.5</entry>
      </row>
      <row id="new-nginx">
	<entry>Nginx<indexterm><primary>Nginx</primary></indexterm></entry>
	<entry>1.14</entry>
	<entry>1.18</entry>
      </row>
<!--
      <row id="new-openldap">
	<entry>OpenLDAP</entry>
	<entry>2.4.44</entry>
	<entry>2.4.47</entry>
      </row>
-->
      <row id="new-openjdk">
	<entry>OpenJDK<indexterm><primary>OpenJDK</primary></indexterm></entry>
	<entry>11</entry>
	<entry>11</entry>
      </row>
      <row id="new-openssh">
	<entry>OpenSSH<indexterm><primary>OpenSSH</primary></indexterm></entry>
	<entry>7.9p1</entry>
	<entry>8.4p1</entry>
      </row>
      <row id="new-perl">
	<entry>Perl<indexterm><primary>Perl</primary></indexterm></entry>
	<entry>5.28</entry>
	<entry>5.32</entry>
      </row>
      <row id="new-php">
	<entry><acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm></entry>
	<entry>7.3</entry>
	<entry>7.4</entry>
      </row>
      <row id="new-postfix">
	<entry>Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>3.4</entry>
	<entry>3.5</entry>
      </row>
      <row id="new-postgresql">
	<entry>PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm></entry>
	<entry>11</entry>
	<entry>13</entry>
      </row>
<!--
      <row id="new-python">
	<entry>Python</entry>
	<entry>2.6</entry>
	<entry>2.7</entry>
      </row>
-->
      <row id="new-python3">
	<entry>Python 3</entry>
	<entry>3.7.3</entry>
	<entry>3.9.1</entry>
      </row>
      <row id="new-rustc">
	<entry>Rustc</entry>
	<entry>1.41 (1.34 for <literal>armel</literal>)</entry>
	<entry>1.48</entry>
      </row>
      <row id="new-samba">
	<entry>Samba</entry>
	<entry>4.9</entry>
	<entry>4.13</entry>
      </row>
      <row id="new-vim">
	<entry>Vim</entry>
	<entry>8.1</entry>
	<entry>8.2</entry>
      </row>
    </tbody>
  </tgroup>
</informaltable>


</section>
</chapter>
