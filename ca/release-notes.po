# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"POT-Creation-Date: 2019-07-07 21:16+0200\n"
"PO-Revision-Date: 2019-06-16 20:06+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <book>
#: en/release-notes.dbk:8
msgid "en"
msgstr "ca"

#. type: Content of: <book><title>
#: en/release-notes.dbk:9
msgid "Release Notes for &debian; &release; (&releasename;), &arch-title;"
msgstr ""
"Notes de llançament per &debian; &release; (&releasename;), &arch-title;"

#. type: Content of: <book><subtitle>
#: en/release-notes.dbk:11
msgid ""
"<ulink url=\"https://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"
msgstr ""
"<ulink url=\"https://www.debian.org/doc/\">El projecte de documentació de "
"Debian</ulink>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:17
msgid ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"
msgstr ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:22
msgid ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"
msgstr ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:27
msgid ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"
msgstr ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:32
msgid ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"
msgstr ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:37
msgid "<firstname></firstname> <surname></surname>"
msgstr "<firstname></firstname> <surname></surname>"

#. type: Content of: <book><bookinfo><editor><contrib>
#: en/release-notes.dbk:39
msgid "There were more people!"
msgstr "Hi ha més gent!"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:43
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."
msgstr ""
"Aquest document és programari lliure, el podeu redistribuir i/o modificar "
"sota els termes de la Llicència Pública General de GNU versió 2 de la "
"llicència, tal com la publica la Free Software Foundation."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:49
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"Aquest programa es distribueix amb l'esperança que serà útil, però sense cap "
"garantia, fins i tot sense la garantia implícita de COMERCIALITACIÓ o "
"IDONEÏTAT PER A UN PROPÒSIT PARTICULAR. Consulteu la llicència general "
"pública de GNU per a més detalls."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:56
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Hauríeu d'haver rebut una còpia de la licència pública general GNU amb este "
"programa; si no, escriviu a la Free Software Foundation, Inc., 51 Franklin "
"Street, Fifth Floor, Boston, MA 02110-1301 USA."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:61
msgid ""
"The license text can also be found at <ulink url=\"https://www.gnu.org/"
"licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
"filename> on &debian; systems."
msgstr ""
"Podeu també trobar el text amb la licència a <ulink url=\"https://www.gnu."
"org/licenses/gpl-2.0.html\"/> i a <filename>/usr/share/common-licenses/"
"GPL-2</filename> en els sistemes &debian;."

#. type: Content of: <book><appendix><title>
#: en/release-notes.dbk:84
msgid "Contributors to the Release Notes"
msgstr "Contribuïdors de les notes de llançament"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:86
msgid ""
"Many people helped with the release notes, including, but not limited to"
msgstr ""
"Molta gent ha ajudat en les notes de llançament, estan incloses, però no "
"limitades a"

#.  alphabetical (LANG=C sort) order by firstname 
#.  the contrib will not be printed, but is a reminder for the editor;
#.          username as shown in vcs log, contribution  
#.  list of translators will only show up in translated texts, only list
#.          contributors to en/ here 
#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:94
#, fuzzy
#| msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgid "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:97
msgid "various fixes in 2013"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:98
#, fuzzy
#| msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgid ""
"</author>, <author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:102 en/release-notes.dbk:217
msgid "previous releases"
msgstr "llançaments anteriors"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:103
msgid ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"
msgstr ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:107
msgid "aba, previous releases: 2005 - 2007"
msgstr "aba, llançaments anteriors: 2005—2007"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:108
msgid ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"
msgstr ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:112 en/release-notes.dbk:152 en/release-notes.dbk:252
#: en/release-notes.dbk:272
msgid "various contributions"
msgstr "contribucions diverses"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:113
msgid ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"
msgstr ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:117 en/release-notes.dbk:122
msgid "previous release"
msgstr "llançament anterior"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:118
msgid ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"
msgstr ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:123
msgid ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"
msgstr ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:127
msgid "description of GM965 issue"
msgstr "descripció del problema GM965"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:128
msgid ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:132
msgid "bubulle, Lenny installation"
msgstr "bubulle, instal·lació de Lenny"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:133
msgid ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"
msgstr ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:137
msgid "PostgreSQL-specific issues"
msgstr "problemes especifics de PostgreSQL"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:138
msgid ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:142
msgid "Debian Live"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:143
msgid ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"
msgstr ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:147
msgid "taffit, Wheezy release"
msgstr "taffit, llançament de Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:148
msgid ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"
msgstr ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:153
msgid ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"
msgstr ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:157
msgid "backports"
msgstr "backports"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:158
msgid ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"
msgstr ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:162
msgid "rework X11 upgrade"
msgstr "reescriptura de l'actualització de les l'X11"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:163
msgid "</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgstr ""
"</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:167
msgid "fjp, previous release (Etch)"
msgstr "fjp, llançament anterior (Etch)"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:168
msgid ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:172 en/release-notes.dbk:262
msgid "innumerable contributions"
msgstr "moltíssimes contribucions"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:173
msgid ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:177 en/release-notes.dbk:242
msgid "ARM port issues"
msgstr "problemes del port d'ARM"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:178
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Hideki</firstname> <surname>Yamane</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:182
msgid "henrich, contributed and contributing since 2006"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:183
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgid ""
"</author>, <author> <firstname>Holger</firstname> <surname>Wansing</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:187
msgid "holgerw, contributed and contributing since 2009"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:188
msgid ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"
msgstr ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:192
msgid "jfs, Etch release, Squeeze release"
msgstr "jfs, llançament d'Etch, llançament d'Squeeze"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:193
msgid ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgstr ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:197
msgid "German translation, innumerable contributions"
msgstr "traducció a l'alemany, moltíssimes contribucions"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:198
msgid ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgstr ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:202 en/release-notes.dbk:247
msgid "syslog issues"
msgstr "problemes del syslog"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:203
msgid ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"
msgstr ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:207
msgid "jrnieder@gmail.com, Squeeze release, Wheezy release"
msgstr "jrnieder@gmail.com, llançament d'Squeeze, llançament de Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:208
msgid ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:212
msgid "joostvb, Wheezy release, Jessie release"
msgstr "joostvb, llançament de Wheezy, llançament de Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:213
msgid ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgstr ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:218
msgid ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"
msgstr ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:222
msgid "jcristau, Squeeze release, Wheezy release"
msgstr "jcristau, llançament d'Squeeze, llançament de Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:223
msgid ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"
msgstr ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:227
msgid "English fixes"
msgstr "correccions en l'anglès"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:228
msgid ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"
msgstr ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:232
msgid "description of NFS issues"
msgstr "descripció dels problemes de l'NFS"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:233
msgid "</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"
msgstr ""
"</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:237
msgid "editors motivation manager"
msgstr "gestor de motivació dels editors"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:238
msgid ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:243
msgid ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"
msgstr ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:248
msgid ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:253
msgid ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"
msgstr ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:257
msgid "nthykier, Jessie release"
msgstr "nthykier, llançament de Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:258
msgid ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"
msgstr ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:263
msgid ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:267
msgid "Japanese translation (coordination), innumerable contributions"
msgstr "traducció al japonès (coordinador), moltíssimes contribucions"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:268
msgid ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"
msgstr ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:273
msgid ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"
msgstr ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:277
msgid "elbrus, buster release"
msgstr "elbrus, llançament de Buster"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:278
msgid ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:282
msgid "kernel version note"
msgstr "nota de la versió del nucli"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:283
msgid ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"
msgstr ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:287 en/release-notes.dbk:312
msgid "Etch release"
msgstr "llançament d'Etch"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:288
msgid ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"
msgstr ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:292 en/release-notes.dbk:297
msgid "description of d-i Braille support"
msgstr "descripció del suport de Braille al d-i"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:293
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:298
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:302
msgid "spaillar-guest, innumerable contributions"
msgstr "spaillar-guest, moltíssimes contribucions"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:303
msgid ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"
msgstr ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:307
msgid "description of Apache issues"
msgstr "descripció dels problemes amb l'Apache"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:308
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:313
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:317
msgid "Debian CDs"
msgstr "CDs de Debian"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:318
msgid ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"
msgstr ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:322 en/release-notes.dbk:331
msgid "description of \"proposed-update\""
msgstr "descripció de \"proposed-update\""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:323
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgid "</author>, <author> <firstname>victory</firstname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:326
msgid ""
"victory-guest victory.deb@gmail.com, markup fixes, contributed and "
"contributing since 2006"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:327
msgid ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:332
msgid ""
"</author>, and <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"
msgstr ""
"</author>, i <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:337
msgid "editing Lenny release, switch to DocBook XML"
msgstr "edició de les notes de llançament de Lenny, migració a DocBook XML"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:338
msgid "</author>."
msgstr "</author>."

#.  translator names here, depending on language!
#.     </para>
#. <para>Translated into Klingon by:
#.     <author>
#.       <firstname>Firstname1</firstname>
#.       <surname>Surname1</surname>
#.       <contrib>Foo translation</contrib>
#.     </author>,
#.     <author>
#.       <firstname>Firstname2</firstname>
#.       <surname>Surname2</surname>
#.       <contrib>Foo translation</contrib>
#.     </author 
#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:341
msgid ""
"This document has been translated into many languages.  Many thanks to the "
"translators!"
msgstr ""
"Este document s'ha traduït a molts idiomes. Moltes gràcies als traductors!"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:364
msgid "ACPI"
msgstr "ACPI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:365
msgid "Advanced Configuration and Power Interface"
msgstr ""
"Advanced Configuration and Power Interface (Configuració avançada i "
"interfície d'energia)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:368
msgid "ALSA"
msgstr "ALSA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:369
msgid "Advanced Linux Sound Architecture"
msgstr ""
"Advanced Linux Sound Architecture (Arquitecutra avançada de só de Linux)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:372
msgid "BD"
msgstr "BD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:373
msgid "Blu-ray Disc"
msgstr "Disc Blu-ray"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:376
msgid "CD"
msgstr "CD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:377
msgid "Compact Disc"
msgstr "Compact Disc (Disc compacte)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:380
msgid "CD-ROM"
msgstr "CD-ROM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:381
msgid "Compact Disc Read Only Memory"
msgstr "Disc compacte de només lectura"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:384
msgid "DHCP"
msgstr "DHCP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:385
msgid "Dynamic Host Configuration Protocol"
msgstr "Protocol de configuració de màquina dinàmic"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:388
msgid "DLBD"
msgstr "DLBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:389
msgid "Dual Layer Blu-ray Disc"
msgstr "Disc Blu-ray de dues capes"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:392
msgid "DNS"
msgstr "DNS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:393
msgid "Domain Name System"
msgstr "Domain Name System (Sistema de noms de domini)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:396
msgid "DVD"
msgstr "DVD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:397
msgid "Digital Versatile Disc"
msgstr "Digital Versatile Disc"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:400
msgid "GIMP"
msgstr "GIMP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:401
msgid "GNU Image Manipulation Program"
msgstr ""
"GNU Image Manipulation Program (Programa de manipulació d'imatges de GNU)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:404
msgid "GNU"
msgstr "GNU"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:405
msgid "GNU's Not Unix"
msgstr "GNU's Not Unix (GNU no és Unix)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:408
msgid "GPG"
msgstr "GPG"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:409
msgid "GNU Privacy Guard"
msgstr "GNU Privacy Guard"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:412
msgid "LDAP"
msgstr "LDAP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:413
msgid "Lightweight Directory Access Protocol"
msgstr ""
"Lightweight Directory Access Protocol (Protocol lleuger d'accés a directori"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:416
msgid "LSB"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:417
msgid "Linux Standard Base"
msgstr "Linux Standard Base"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:420
msgid "LVM"
msgstr "LVM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:421
msgid "Logical Volume Manager"
msgstr "Logical Volume Manager (Gestor de volums lògics)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:424
msgid "MTA"
msgstr "MTA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:425
msgid "Mail Transport Agent"
msgstr "Mail Transport Agent (Agent de transport de correu)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:428
msgid "NBD"
msgstr "NBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:429
msgid "Network Block Device"
msgstr "Network Block Device (Dispositiu de block en xarxa)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:432
msgid "NFS"
msgstr "NFS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:433
msgid "Network File System"
msgstr "Network File System (Sistema de fitxers en xarxa)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:436
msgid "NIC"
msgstr "NIC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:437
msgid "Network Interface Card"
msgstr "Network Interface Card (Targeta d'interfície de xarxa)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:440
msgid "NIS"
msgstr "NIS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:441
msgid "Network Information Service"
msgstr "Network Information Service (Servei d'informació de xarxa)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:444
msgid "PHP"
msgstr "PHP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:445
msgid "PHP: Hypertext Preprocessor"
msgstr "PHP: Processador d'hipertext"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:448
msgid "RAID"
msgstr "RAID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:449
msgid "Redundant Array of Independent Disks"
msgstr ""
"Redundant Array of Independent Disks (Matriu redundant de discs independents)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:452
msgid "SATA"
msgstr "SATA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:453
msgid "Serial Advanced Technology Attachment"
msgstr ""
"Serial Advanced Technology Attachment (Tecnologia avançada d'accessoris en "
"serie)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:456
msgid "SSL"
msgstr "SSL"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:457
msgid "Secure Sockets Layer"
msgstr "Secure Sockets Layer (Capa de connexió segura)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:460
msgid "TLS"
msgstr "TLS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:461
msgid "Transport Layer Security"
msgstr "Transport Layer Security (Capa de seguretat de transport)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:464
msgid "UEFI"
msgstr "UEFI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:465
msgid "Unified Extensible Firmware Interface"
msgstr ""
"Unified Extensible Firmware Interface (Interfície unificada i extensible de "
"microprogramari)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:468
msgid "USB"
msgstr "USB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:469
msgid "Universal Serial Bus"
msgstr "Universal Serial Bus (Bus sèrie universal)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:472
msgid "UUID"
msgstr "UUID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:473
msgid "Universally Unique Identifier"
msgstr "Universally Unique Identifier (Identificador únic universal)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:476
msgid "WPA"
msgstr "WPA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:477
msgid "Wi-Fi Protected Access"
msgstr "Wi-Fi Protected Access (Accés protegit a Wi-Fi)"
