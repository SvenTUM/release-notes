# Vietnamese translation for Release Notes (Old Stuff).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2009-02-04 16:55+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.8\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "vi"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
#, fuzzy
#| msgid "Managing your &oldreleasename; system"
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Quản lý hệ thống &oldreleasename;"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Phụ lục này chứa thông tin về cách kiểm tra có thể cài đặt hoặc nâng cấp các "
"gói &oldreleasename; trước khi nâng cấp lên &releasename;. Việc kiểm tra này "
"chỉ nên làm trong một số trường hợp nào đó."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Nâng cấp hệ thống &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
#, fuzzy
#| msgid ""
#| "Basically this is no different than any other upgrade of &oldreleasename; "
#| "you've been doing.  The only difference is that you first need to make "
#| "sure your package list still contains references to &oldreleasename; as "
#| "explained in <xref linkend=\"old-sources\"/>."
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Về cơ bản thì việc này không phải khác với nâng cấp bình thường. Sự khác duy "
"nhất là trước tiên người dùng nên kiểm tra danh sách các gói vẫn còn chứa "
"tham chiếu đến &oldreleasename;, như diễn tả trong <xref linkend=\"old-"
"sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Nếu người dùng nâng cấp hệ thống dùng một máy nhân bản Debian thì hệ thống "
"được tự động nâng cấp lên bản phát hành điểm &oldreleasename; mới nhất."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your sources list"
msgstr "Kiểm tra danh sách nguồn"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#, fuzzy
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to 'stable', you are effectively already <quote>using</quote> "
#| "&releasename;.  If you have already run <literal>apt-get update</"
#| "literal>, you can still get back without problems following the procedure "
#| "below."
msgid ""
"If any of the lines in your <filename>/etc/apt/sources.list</filename> refer "
"to 'stable', you are effectively already <quote>using</quote> &releasename;. "
"This might not be what you want if you are not ready yet for the upgrade.  "
"If you have already run <literal>apt-get update</literal>, you can still get "
"back without problems by following the procedure below."
msgstr ""
"Nếu tập tin <filename>/etc/apt/sources.list</filename> chứa bất cứ dòng nào "
"tham chiếu đến « stable » (bản ổn định) thì kết quả là người dùng đã "
"<quote>sử dụng</quote> &releasename;. Nếu người dùng đã chạy câu lệnh cập "
"nhật <literal>apt-get update</literal>, vẫn còn có thể trở về mà không gặp "
"vấn đề, bằng cách theo thủ tục dưới đây."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Nếu người dùng cũng đã cài đặt gói từ &releasename;, rất có thể không có ích "
"khi cài đặt thêm gói từ &oldreleasename;. Trong trường hợp đó, người dùng "
"cần phải tự quyết định có nên tiếp tục hay không. Cũng có thể hạ cấp gói, "
"nhưng quá trình đó không phải được diễn tả ở đây."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"Open the file <filename>/etc/apt/sources.list</filename> with your favorite "
"editor (as <literal>root</literal>) and check all lines beginning with "
"<literal>deb http:</literal> or <literal>deb ftp:</literal> for a reference "
"to <quote><literal>stable</literal></quote>.  If you find any, change "
"<literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Hãy mở tập tin <filename>/etc/apt/sources.list</filename> dùng trình soạn "
"thảo (với quyền chủ [<literal>root</literal>]) và kiểm tra mọi dòng bắt đầu "
"với <literal>deb http:</literal> hay <literal>deb ftp:</literal> tìm một "
"tham chiếu đến <quote><literal>stable</literal></quote> (bản ổn định). Tìm "
"được thì thay đổi <literal>stable</literal> thành <literal>&oldreleasename;</"
"literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:53
msgid ""
"If you have any lines starting with <literal>deb file:</literal>, you will "
"have to check for yourself if the location they refer to contains an "
"&oldreleasename; or a &releasename; archive."
msgstr ""
"Nếu tập tin đó chứa dòng nào bắt đầu với <literal>deb file:</literal>, người "
"dùng cần phải tự kiểm tra nếu địa chỉ đã tham chiếu có một kho gói kiểu "
"&oldreleasename; hay &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:59
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
"Doing so would invalidate the line and you would have to run <command>apt-"
"cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers to "
"<quote><literal>unstable</literal></quote>.  Although confusing, this is "
"normal."
msgstr ""
"Không nên thay đổi dòng nào bắt đầu với <literal>deb cdrom:</literal>. Thay "
"đổi dòng kiểu nào sẽ làm mất hiệu lực dòng đó thì người dùng cần phải chạy "
"lại câu lệnh <command>apt-cdrom</command>. Không cần lo nếu một dòng nguồn « "
"cdrom » tham chiếu đến <quote><literal>unstable</literal></quote> (bản bất "
"định). Dù trường hợp này làm cho lộn xộn, nó vẫn còn là hợp lệ."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:67
msgid "If you've made any changes, save the file and execute"
msgstr "Sau khi thay đổi gì thì lưu lại tập tin và thực hiện câu lệnh cập nhật"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:70
#, no-wrap
msgid "# apt-get update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "to refresh the package list."
msgstr "để cập nhật danh sách các gói."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:78
msgid "Removing obsolete configuration files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:80
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:88
msgid "Upgrade legacy locales to UTF-8"
msgstr ""

#. type: Content of: <appendix><section><para><footnote><para>
#: en/old-stuff.dbk:92
msgid ""
"In the GNOME screensaver, using passwords with non-ASCII characters, "
"pam_ldap support, or even the ability to unlock the screen may be unreliable "
"when not using UTF-8.  The GNOME screenreader is affected by bug <ulink url="
"\"http://bugs.debian.org/599197\">#599197</ulink>.  The Nautilus file "
"manager (and all glib-based programs, and likely all Qt-based programs too) "
"assume that filenames are in UTF-8, while the shell assumes they are in the "
"current locale's encoding. In daily use, non-ASCII filenames are just "
"unusable in such setups.  Furthermore, the gnome-orca screen reader (which "
"grants sight-impaired users access to the GNOME desktop environment) "
"requires a UTF-8 locale since Squeeze; under a legacy characterset, it will "
"be unable to read out window information for desktop elements such as "
"Nautilus/GNOME Panel or the Alt-F1 menu."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:90
msgid ""
"If your system is localized and is using a locale that is not based on UTF-8 "
"you should strongly consider switching your system over to using UTF-8 "
"locales.  In the past, there have been bugs<placeholder type=\"footnote\" id="
"\"0\"/> identified that manifest themselves only when using a non-UTF-8 "
"locale. On the desktop, such legacy locales are supported through ugly hacks "
"in the library internals, and we cannot decently provide support for users "
"who still use them."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:110
msgid ""
"To configure your system's locale you can run <command>dpkg-reconfigure "
"locales</command>. Ensure you select a UTF-8 locale when you are presented "
"with the question asking which locale to use as a default in the system.  In "
"addition, you should review the locale settings of your users and ensure "
"that they do not have legacy locale definitions in their configuration "
"environment."
msgstr ""
