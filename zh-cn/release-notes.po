#
# Simplified Chinese translation of Debian release notes
# Copyright (C) 2008 Debian Chinese project
# This file is distributed under the same license as the release
# notes.
#
# Authors:
# chenxianren <chenxianren@gmail.com>, 2008.
# Dongsheng Song <dongsheng.song@gmail.com>, 2008-2009.
# LI Daobing <lidaobing@gmail.com>, 2008.
# Deng Xiyue <manphiz@gmail.com>, 2009.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <byang@debian.org>, 2019.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10.0\n"
"POT-Creation-Date: 2019-07-11 20:53+0800\n"
"PO-Revision-Date: 2019-07-08 00:22+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.3\n"

#. type: Attribute 'lang' of: <book>
#: en/release-notes.dbk:8
msgid "en"
msgstr "zh_CN"

#. type: Content of: <book><title>
#: en/release-notes.dbk:9
msgid "Release Notes for &debian; &release; (&releasename;), &arch-title;"
msgstr "&debian; &release; (&releasename;), &arch-title; 的发行说明"

#. type: Content of: <book><subtitle>
#: en/release-notes.dbk:11
msgid ""
"<ulink url=\"https://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"
msgstr "<ulink url=\"https://www.debian.org/doc/\">Debian 文档项目</ulink>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:17
msgid ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"
msgstr ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:22
msgid ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"
msgstr ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:27
msgid ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"
msgstr ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:32
msgid ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"
msgstr ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:37
msgid "<firstname></firstname> <surname></surname>"
msgstr "<firstname></firstname> <surname></surname>"

#. type: Content of: <book><bookinfo><editor><contrib>
#: en/release-notes.dbk:39
msgid "There were more people!"
msgstr "还有更多的人！"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:43
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."
msgstr ""
"本文档是自由软件；您可以在自由软件基金会发布的 GNU 通用公共许可证的条款下重新"
"发布或修改它；您应当使用该许可证的第二版本。"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:49
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"本程序发布的目的是希望它对您有用，但没有任何担保，甚至不保证它有经济价值和适"
"合特定用途。请查阅 GNU 通用公共许可证以获得更多细节。"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:56
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"您应当在收到本程序的同时也收到了一份 GNU 通用公共许可证的副本；如果没有收到，"
"请给自由软件基金会写信。地址是：51 Franklin Street, Fifth Floor, Boston, MA "
"02110-1301 USA."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:61
msgid ""
"The license text can also be found at <ulink url=\"https://www.gnu.org/"
"licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
"filename> on &debian; systems."
msgstr ""
"协议文本可以在 <ulink url=\"https://www.gnu.org/licenses/gpl-2.0.html\"/> 和 "
"&debian; 系统中的 <filename>/usr/share/common-licenses/GPL-2</filename> 找"
"到。"

#. type: Content of: <book><appendix><title>
#: en/release-notes.dbk:84
msgid "Contributors to the Release Notes"
msgstr "发行说明的贡献者"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:86
msgid ""
"Many people helped with the release notes, including, but not limited to"
msgstr "有许多人对发行说明提供了帮助，包括但不限于"

#.  alphabetical (LANG=C sort) order by firstname 
#.  the contrib will not be printed, but is a reminder for the editor;
#.          username as shown in vcs log, contribution  
#.  list of translators will only show up in translated texts, only list
#.          contributors to en/ here 
#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:94
msgid "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:97
msgid "various fixes in 2013"
msgstr "2013 年的多项修复"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:98
msgid ""
"</author>, <author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgstr ""
"</author>，<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:102 en/release-notes.dbk:217
msgid "previous releases"
msgstr "先前的版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:103
msgid ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"
msgstr ""
"</author>，<author><firstname>Andreas</firstname> <surname>Barth</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:107
msgid "aba, previous releases: 2005 - 2007"
msgstr "aba，先前的版本：2005 - 2007"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:108
msgid ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"
msgstr "</author>，<author><firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:112 en/release-notes.dbk:152 en/release-notes.dbk:252
#: en/release-notes.dbk:272
msgid "various contributions"
msgstr "大量贡献"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:113
msgid ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"
msgstr ""
"</author>，<author><firstname>Anne</firstname> <surname>Bezemer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:117 en/release-notes.dbk:122
msgid "previous release"
msgstr "之前的发行"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:118
msgid ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"
msgstr ""
"</author>，<author><firstname>Bob</firstname> <surname>Hilliard</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:123
msgid ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"
msgstr ""
"</author>，<author><firstname>Charles</firstname> <surname>Plessy</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:127
msgid "description of GM965 issue"
msgstr "GM965 问题描述"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:128
msgid ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"
msgstr ""
"</author>，<author><firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:132
msgid "bubulle, Lenny installation"
msgstr "bubulle，Lenny 安装程序"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:133
msgid ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"
msgstr ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:137
msgid "PostgreSQL-specific issues"
msgstr "PostgreSQL 相关的问题"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:138
msgid ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"
msgstr ""
"</author>，<author><firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:142
msgid "Debian Live"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:143
msgid ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"
msgstr ""
"</author>，<author><firstname>David</firstname> <surname>Prévot</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:147
msgid "taffit, Wheezy release"
msgstr "taffit，Wheezy 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:148
msgid ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"
msgstr ""
"</author>，<author><firstname>Emmanuel</firstname> <surname>Petrișor</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:153
msgid ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"
msgstr ""
"</author>，<author><firstname>Emmanuel</firstname> <surname>Kasper</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:157
msgid "backports"
msgstr "回迁软件包"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:158
msgid ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"
msgstr ""
"</author>，<author><firstname>Esko</firstname> <surname>Arajärvi</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:162
msgid "rework X11 upgrade"
msgstr "重新实现 X11 升级"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:163
msgid "</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgstr "</author>，<author><firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:167
msgid "fjp, previous release (Etch)"
msgstr "fjp，先前版本（Etch）"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:168
msgid ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"
msgstr ""
"</author>，<author><firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:172 en/release-notes.dbk:262
msgid "innumerable contributions"
msgstr "无数贡献"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:173
msgid ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"
msgstr ""
"</author>，<author><firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:177 en/release-notes.dbk:242
msgid "ARM port issues"
msgstr "ARM 移植的问题"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:178
msgid ""
"</author>, <author> <firstname>Hideki</firstname> <surname>Yamane</surname>"
msgstr ""
"</author>，<author> <firstname>Hideki</firstname> <surname>Yamane</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:182
msgid "henrich, contributed and contributing since 2006"
msgstr "henrich，自 2006 年起"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:183
msgid ""
"</author>, <author> <firstname>Holger</firstname> <surname>Wansing</surname>"
msgstr ""
"</author>，<author> <firstname>Holger</firstname> <surname>Wansing</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:187
msgid "holgerw, contributed and contributing since 2009"
msgstr "holgerw，自 2009 年起"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:188
msgid ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"
msgstr ""
"</author>，<author><firstname>Javier</firstname> <surname>Fernández-Sanguino "
"Peña</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:192
msgid "jfs, Etch release, Squeeze release"
msgstr "jfs，Etch 版本，Squeeze 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:193
msgid ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgstr ""
"</author>，<author><firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:197
msgid "German translation, innumerable contributions"
msgstr "德语翻译者，无数贡献"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:198
msgid ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgstr ""
"</author>，<author><firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:202 en/release-notes.dbk:247
msgid "syslog issues"
msgstr "syslog 问题"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:203
msgid ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"
msgstr ""
"</author>，<author><firstname>Jonathan</firstname> <surname>Nieder</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:207
msgid "jrnieder@gmail.com, Squeeze release, Wheezy release"
msgstr "jrnieder@gmail.com，Squeeze 版本，Wheezy 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:208
msgid ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"
msgstr ""
"</author>，<author><firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:212
msgid "joostvb, Wheezy release, Jessie release"
msgstr "joostvb，Wheezy 版本，Jessie 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:213
msgid ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgstr ""
"</author>，<author><firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:218
msgid ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"
msgstr ""
"</author>，<author><firstname>Julien</firstname> <surname>Cristau</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:222
msgid "jcristau, Squeeze release, Wheezy release"
msgstr "jcristau，Squeeze 版本，Wheezy 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:223
msgid ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"
msgstr ""
"</author>，<author><firstname>Justin B</firstname> <surname>Rye</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:227
msgid "English fixes"
msgstr "英语修改"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:228
msgid ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"
msgstr ""
"</author>，<author><firstname>LaMont</firstname> <surname>Jones</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:232
msgid "description of NFS issues"
msgstr "NFS 问题的描述"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:233
msgid "</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"
msgstr "</author>，<author><firstname>Luk</firstname> <surname>Claes</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:237
msgid "editors motivation manager"
msgstr "编辑动员管理员"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:238
msgid ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"
msgstr ""
"</author>，<author><firstname>Martin</firstname> <surname>Michlmayr</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:243
msgid ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"
msgstr ""
"</author>，<author><firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:248
msgid ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"
msgstr ""
"</author>，<author><firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:253
msgid ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"
msgstr ""
"</author>，<author><firstname>Niels</firstname> <surname>Thykier</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:257
msgid "nthykier, Jessie release"
msgstr "nthykier，Jessie 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:258
msgid ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"
msgstr ""
"</author>，<author><firstname>Noah</firstname> <surname>Meyerhans</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:263
msgid ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"
msgstr ""
"</author>，<author><firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:267
msgid "Japanese translation (coordination), innumerable contributions"
msgstr "日语翻译（协调），无数贡献"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:268
msgid ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"
msgstr ""
"</author>，<author><firstname>Osamu</firstname> <surname>Aoki</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:273
msgid ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"
msgstr ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:277
msgid "elbrus, buster release"
msgstr "elbrus, buster 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:278
msgid ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgstr ""
"</author>，<author><firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:282
msgid "kernel version note"
msgstr "内核版本注记"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:283
msgid ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"
msgstr ""
"</author>，<author><firstname>Rob</firstname> <surname>Bradford</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:287 en/release-notes.dbk:312
msgid "Etch release"
msgstr "Etch 版本"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:288
msgid ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"
msgstr ""
"</author>，<author><firstname>Samuel</firstname> <surname>Thibault</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:292 en/release-notes.dbk:297
msgid "description of d-i Braille support"
msgstr "d-i 布莱叶支持描述"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:293
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"
msgstr ""
"</author>，<author><firstname>Simon</firstname> <surname>Bienlein</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:298
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"
msgstr ""
"</author>，<author><firstname>Simon</firstname> <surname>Paillard</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:302
msgid "spaillar-guest, innumerable contributions"
msgstr "spaillar-guest，无数贡献"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:303
msgid ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"
msgstr ""
"</author>，<author><firstname>Stefan</firstname> <surname>Fritsch</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:307
msgid "description of Apache issues"
msgstr "Apache 问题描述"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:308
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"
msgstr ""
"</author>，<author><firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:313
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"
msgstr ""
"</author>，<author><firstname>Steve</firstname> <surname>McIntyre</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:317
msgid "Debian CDs"
msgstr "Debian CD"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:318
msgid ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"
msgstr ""
"</author>，<author><firstname>Tobias</firstname> <surname>Scherer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:322 en/release-notes.dbk:331
msgid "description of \"proposed-update\""
msgstr "“proposed-update”描述"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:323
msgid "</author>, <author> <firstname>victory</firstname>"
msgstr "</author>，<author> <firstname>victory</firstname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:326
msgid ""
"victory-guest victory.deb@gmail.com, markup fixes, contributed and "
"contributing since 2006"
msgstr "victory-guest victory.deb@gmail.com，标记语言修复，自 2006 年起"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:327
msgid ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"
msgstr ""
"</author>，<author><firstname>Vincent</firstname> <surname>McIntyre</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:332
msgid ""
"</author>, and <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"
msgstr ""
"</author>，和 <author><firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:337
msgid "editing Lenny release, switch to DocBook XML"
msgstr "编辑 Lenny 发行，转换到 DocBook XML"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:338
msgid "</author>."
msgstr "</author>。"

#
#.  translator names here, depending on language!
#.     </para>
#. <para>Translated into Klingon by:
#.     <author>
#.       <firstname>Firstname1</firstname>
#.       <surname>Surname1</surname>
#.       <contrib>Foo translation</contrib>
#.     </author>,
#.     <author>
#.       <firstname>Firstname2</firstname>
#.       <surname>Surname2</surname>
#.       <contrib>Foo translation</contrib>
#.     </author 
#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:341
msgid ""
"This document has been translated into many languages.  Many thanks to the "
"translators!"
msgstr "本文档被翻译为多种语言。感谢这些翻译者们！"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:364
msgid "ACPI"
msgstr "ACPI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:365
msgid "Advanced Configuration and Power Interface"
msgstr "高级配置和电源接口（Advanced Configuration and Power Interface）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:368
msgid "ALSA"
msgstr "ALSA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:369
msgid "Advanced Linux Sound Architecture"
msgstr "高级 Linux 声音架构（Advanced Linux Sound Architecture）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:372
msgid "BD"
msgstr "BD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:373
msgid "Blu-ray Disc"
msgstr "蓝光光碟（Blu-ray Disc）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:376
msgid "CD"
msgstr "CD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:377
msgid "Compact Disc"
msgstr "小型光碟（Compact Disc）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:380
msgid "CD-ROM"
msgstr "CD-ROM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:381
msgid "Compact Disc Read Only Memory"
msgstr "只读光碟（Compact Disc Read Only Memory）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:384
msgid "DHCP"
msgstr "DHCP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:385
msgid "Dynamic Host Configuration Protocol"
msgstr "动态主机配置协议（Dynamic Host Configuration Protocol）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:388
msgid "DLBD"
msgstr "DLBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:389
msgid "Dual Layer Blu-ray Disc"
msgstr "双层蓝光光碟（Dual Layer Blu-ray Disc）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:392
msgid "DNS"
msgstr "DNS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:393
msgid "Domain Name System"
msgstr "域名系统（Domain Name System）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:396
msgid "DVD"
msgstr "DVD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:397
msgid "Digital Versatile Disc"
msgstr "数字通用光碟（Digital Versatile Disc）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:400
msgid "GIMP"
msgstr "GIMP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:401
msgid "GNU Image Manipulation Program"
msgstr "GNU 图像处理程序（GNU Image Manipulation Program）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:404
msgid "GNU"
msgstr "GNU"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:405
msgid "GNU's Not Unix"
msgstr "GNU's Not Unix"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:408
msgid "GPG"
msgstr "GPG"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:409
msgid "GNU Privacy Guard"
msgstr "GNU 隐私保护（GNU Privacy Guard）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:412
msgid "LDAP"
msgstr "LDAP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:413
msgid "Lightweight Directory Access Protocol"
msgstr "轻量目录访问协议（Lightweight Directory Access Protocol）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:416
msgid "LSB"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:417
msgid "Linux Standard Base"
msgstr "Linux 标准基础（Linux Standard Base）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:420
msgid "LVM"
msgstr "LVM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:421
msgid "Logical Volume Manager"
msgstr "逻辑卷管理器（Logical Volume Manager）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:424
msgid "MTA"
msgstr "MTA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:425
msgid "Mail Transport Agent"
msgstr "邮件传送代理（Mail Transport Agent）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:428
msgid "NBD"
msgstr "NBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:429
msgid "Network Block Device"
msgstr "网络块设备（Network Block Device）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:432
msgid "NFS"
msgstr "NFS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:433
msgid "Network File System"
msgstr "网络文件系统（Network File System）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:436
msgid "NIC"
msgstr "NIC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:437
msgid "Network Interface Card"
msgstr "网络接口卡（Network Interface Card）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:440
msgid "NIS"
msgstr "NIS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:441
msgid "Network Information Service"
msgstr "网络信息服务（Network Information Service）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:444
msgid "PHP"
msgstr "PHP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:445
msgid "PHP: Hypertext Preprocessor"
msgstr "PHP：超文本预处理器（PHP: Hypertext Preprocessor）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:448
msgid "RAID"
msgstr "RAID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:449
msgid "Redundant Array of Independent Disks"
msgstr "独立磁盘冗余阵列（Redundant Array of Independent Disks）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:452
msgid "SATA"
msgstr "SATA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:453
msgid "Serial Advanced Technology Attachment"
msgstr "串行先进技术总线附属（Serial Advanced Technology Attachment）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:456
msgid "SSL"
msgstr "SSL"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:457
msgid "Secure Sockets Layer"
msgstr "安全套接字层（Secure Sockets Layer）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:460
msgid "TLS"
msgstr "TLS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:461
msgid "Transport Layer Security"
msgstr "传输层安全协议（Transport Layer Security）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:464
msgid "UEFI"
msgstr "UEFI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:465
msgid "Unified Extensible Firmware Interface"
msgstr "统一可扩展固件接口（Unified Extensible Firmware Interface）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:468
msgid "USB"
msgstr "USB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:469
msgid "Universal Serial Bus"
msgstr "通用串行总线（Universal Serial Bus）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:472
msgid "UUID"
msgstr "UUID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:473
msgid "Universally Unique Identifier"
msgstr "通用惟一标识符（Universally Unique Identifier）"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:476
msgid "WPA"
msgstr "WPA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:477
msgid "Wi-Fi Protected Access"
msgstr "Wi-Fi 保护接入（Wi-Fi Protected Access）"

#~ msgid "APM"
#~ msgstr "APM"

#~ msgid "Advanced Power Management"
#~ msgstr "高级电源管理"

#~ msgid "IDE"
#~ msgstr "IDE"

#~ msgid "Integrated Drive Electronics"
#~ msgstr "电子集成驱动器"

#~ msgid "LILO"
#~ msgstr "LILO"

#~ msgid "LInux LOader"
#~ msgstr "Linux 加载器"

#~ msgid "OSS"
#~ msgstr "OSS"

#~ msgid "Open Sound System"
#~ msgstr "开放声音系统"

#~ msgid "RPC"
#~ msgstr "RPC"

#~ msgid "Remote Procedure Call"
#~ msgstr "远程过程调用"

#~ msgid "VGA"
#~ msgstr "VGA"

#~ msgid "Video Graphics Array"
#~ msgstr "视频图像阵列"

#, fuzzy
#~| msgid "previous release"
#~ msgid "previous release (Etch)"
#~ msgstr "之前的发行"

#~ msgid "</editor>"
#~ msgstr "</editor>"

#, fuzzy
#~| msgid "2009-02-01"
#~ msgid "2010-11-12"
#~ msgstr "2009年2月1日"

#~ msgid "Lenny dedicated to Thiemo Seufer"
#~ msgstr "Lenny 献给 Thiemo Seufer"

#~ msgid ""
#~ "The Debian Project has lost an active member of its community. Thiemo "
#~ "Seufer died on December 26th, 2008 in a tragic car accident."
#~ msgstr ""
#~ "Debian 项目失去了一位积极的社区成员。Thiemo Seufer 在2008年12月26日的车祸"
#~ "中丧生。"

#~ msgid ""
#~ "Thiemo was involved in Debian in many ways. He maintained several "
#~ "packages and was the main supporter of the Debian ports to the MIPS "
#~ "architecture. He was also a member of our kernel team, as well as a "
#~ "member of the Debian Installer team. His contributions reached far beyond "
#~ "the Debian project: he also worked on the MIPS port of the Linux kernel, "
#~ "the MIPS emulation of qemu, and far too many smaller projects to be named "
#~ "here."
#~ msgstr ""
#~ "Thiemo 用多种方式参与了 Debian 计划。他维护着多个软件包并是 MIPS 的 "
#~ "Debian 移植的主要支持者。他还是我们内核组及 Debian 安装程序组的成员。他的"
#~ "贡献远远超出 Debian 计划。他还致力于 Linux 内核的 MIPS 移植以及 qemu 的 "
#~ "MIPS 仿真等工作，同时还参与多个小项目，这里难以一一述及。"

#~ msgid ""
#~ "Thiemo's work, dedication, broad technical knowledge and ability to share "
#~ "this with others will be missed. His contributions will not be "
#~ "forgotten.  The high standards of Thiemo's work make it hard to pick up."
#~ msgstr ""
#~ "我们将永远怀念 Thiemo 的工作、奉献以及他同其他人分享的广泛的技术知识与能"
#~ "力。Thiemo 的贡献不会被遗忘。Thiemo\n"
#~ "工作的高标准我们永难企及。"

#~ msgid ""
#~ "To honour his contributions to Debian, the project dedicates the release "
#~ "of Debian GNU/Linux 5.0 <quote>Lenny</quote> to Thiemo."
#~ msgstr ""
#~ "为了尊敬他对 Debian 的贡献，项目将 Debian GNU/Linux 5.0 <quote>Lenny</"
#~ "quote> 发行献给 Thiemo。"
