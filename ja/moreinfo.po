# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# Hideki Yamane <henrich@debian.org>, 2010-2019
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2019-06-01 13:06+0900\n"
"PO-Revision-Date: 2019-06-15 22:05+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "ja"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "&debian; に関するさらなる情報"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "もっと読みたい"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"このリリースノートやインストールガイドを越えた、Debian に関するより詳細な文書"
"が、Debian Documentation Project (DDP) から公開されています。DDP は Debian の"
"ユーザや開発者向けに、Debian リファレンス・Debian 新メンテナガイド・Debian "
"FAQ などなどの様に、品質の高い文書を作成することを目的としています。現在利用"
"可能なリソースのすべてについて、詳細は <ulink url=\"&url-ddp;\">DDP のウェブ"
"サイト</ulink>および<ulink url=\"&url-wiki;\">Debian Wiki</ulink> を参照して"
"下さい。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"それぞれのパッケージの文書は <filename>/usr/share/doc/<replaceable>パッケージ"
"</replaceable></filename>にインストールされています。ここには、著作権情報、"
"Debian 固有の詳細、開発元の文書すべて、などが置かれています。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "助けを求めるには"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Debian ユーザ向けのヘルプ・アドバイス・サポートなどは、いろいろな場所から得ら"
"れます。しかしこれらを頼りにするのは、入手できるドキュメントでその問題につい"
"て調査してからにしましょう。この章では新しく Debian ユーザになった人にとって"
"役立つであろう、これらのリソースを簡単に紹介します。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "メーリングリスト"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Debian ユーザが最も興味を引かれるであろうメーリングリストは debian-user (英"
"語) リストおよび debian-user-<replaceable>言語</replaceable> (各国語) リスト"
"でしょう。これらのリストの詳細や講読のしかたについては、<ulink url=\"&url-"
"debian-list-archives;\"></ulink> を見てください。利用にあたっては、あなたの疑"
"問に対する答えが以前の投稿ですでに答えられていないかどうか、アーカイブを"
"チェックしてください。また標準的なメーリングリストのエチケットに従うようにし"
"てください。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "インターネットリレーチャット (IRC)"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian には、Debian ユーザのサポートや援助のために専用の IRC チャンネルが "
"OFTC IRC ネットワークにあります。このチャンネルにアクセスするには、お好みの "
"IRC クライアントを irc.debian.org に接続し、<literal>#debian</literal> に "
"join してください。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"チャンネルのガイドラインに従い、他のユーザをきちんと尊重してください。ガイド"
"ラインは <ulink url=\"&url-wiki;DebianIRC\">Debian Wiki</ulink> で参照できま"
"す。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"OFTC についてさらに詳しく知りたい場合は、<ulink url=\"&url-irc-host;\">ウェブ"
"サイト</ulink>を訪ねてみてください。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "バグを報告する"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"私たちは Debian を高品質な OS にするよう努めていますが、だからといって私たち"
"の提供するパッケージにバグが皆無というわけではありません。Debian の <quote>"
"オープンな開発体制</quote> という考え方に合致し、また、ユーザに対するサービス"
"として、私たちは報告されたバグに関するすべての情報をバグ追跡システム (Bug "
"Tracking System: BTS) で提供しています。この BTS は <ulink url=\"&url-bts;"
"\"></ulink> で閲覧できます"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"もしディストリビューションや、その一部であるパッケージされたソフトウェアにバ"
"グを見つけたら、将来のリリースで修正できるよう、その問題点の報告をお願いしま"
"す。バグを報告するには有効な電子メールアドレスが必要です。これをお願いしてい"
"るのは、バグを追跡できるようにするため、そして追加情報が必要になった場合に開"
"発者が報告者に連絡できるようにするためです。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"バグ報告は、<command>reportbug</command> プログラムを使って送信することもでき"
"ますし、電子メールを使って手で送ることもできます。バグ追跡システムに関する詳"
"細やその使い方については、リファレンス文書 (<systemitem role=\"package\">doc-"
"debian</systemitem> パッケージをインストールしていれば <filename>/usr/share/"
"doc/debian</filename> にあります) をお読み頂くか、または<ulink url=\"&url-"
"bts;\">バグ追跡システム</ulink>のウェブサイトからオンラインで入手することもで"
"きます。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Debian に貢献する"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Debian への貢献は専門家でなくてもできます。問題を抱えたユーザーを、いろいろなサ"
"ポート <ulink url=\"&url-debian-list-archives;\">メーリングリスト</ulink> で"
"助けてあげることも、立派なコミュニティへの貢献です。開発 <ulink url=\"&url-"
"debian-list-archives;\">メーリングリスト</ulink> に参加して、ディストリビュー"
"ション開発に関する問題を見つける (そして解決する) ことも、もちろん非常に助け"
"になります。Debian を高品質なディストリビューションに保つため、<ulink url="
"\"&url-bts;\">バグを報告して</ulink>その原因の特定や解決に際して開発者を助け"
"てください。<systemitem role=\"package\">how-can-i-help</systemitem> という"
"ツールが作業するのに適した報告済みのバグを探すのに役立つでしょう。執筆が得意"
"なら、<ulink url=\"&url-ddp-vcs-info;\">文書</ulink>作成や既存文書の自分の言語への"
"<ulink url=\"&url-debian-i18n;\">翻訳</ulink>に積極的に参加し、そこで貢献する"
"のもよいでしょう。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"もっと時間が自由になるなら、Debian に属するフリーソフトウェア集の一部を管理し"
"てみるのはどうでしょうか。皆が Debian に入れてほしいと思っているソフトウェア"
"を引き受けて管理するのは、特に価値の高い貢献です。これに関する詳細は、<ulink "
"url=\"&url-wnpp;\">作業が望まれるパッケージのデータベース</ulink>をご覧になっ"
"てください。Debian にはいくつか<ulink url=\"&url-debian-projects;\">サブプロ"
"ジェクト</ulink>が存在しており、特定のアーキテクチャへの移植や、特定のユーザー"
"層向けの <ulink url=\"&url-debian-blends;\">Debian Pure Blends</ulink> などが"
"あります。これらのうち、あなたが興味を持っているグループに参加するのもよいで"
"しょう。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"いずれにしても、あなたが何らかの形でフリーソフトウェアコミュニティに関わって"
"いるのなら、それがユーザとしてであれ、プログラマー、ライター、翻訳者のいずれと"
"してであれ、すでにあなたはフリーソフトウェア運動を助けてくださっているので"
"す。貢献することは報いのあることですし、楽しいことです。新しい人々に出会う機"
"会も増えます。きっと暖かで楽しい気持ちになれるはずです。"
